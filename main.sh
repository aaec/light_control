#!/bin/bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#	Ava Cole
# 	aaec@uw.edu
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# makes a interactive interface for setting the light strips. Requires root permissions
# in order to set the gpio

# This script contains all of the functions for different color routines
source /home/pi/light_control/colors.sh

# ------------ CONSTANTS -----------
SLEEPTIME='.5'
PINSFILE='/home/pi/light_control/pins.txt'
ALLPINS="$(cat $PINSFILE | awk 'NR != 1 { print $3 }')"

# ------------ FUNCTIONS -----------

# set pin $1 to value $2
function setpin {
	echo $2 > /sys/class/gpio/gpio$1/value
}

# returns all of the pins associated with strip $1
function getpins {
	echo "$(cat $PINSFILE | grep ^"$1" | awk '{ print $3 }')"
}

# set the strip $1 to the color $2 $3 $4. $2-3 are ints between 0-255
# DOES NOT WORK YET
function setstrip {
	# get the pins associated with the strip $1
	param=1
	for pin in `getpins $1`;
	do
		setpin $pin ${param}
		((param++))
	done
}

function startup {
	for pin in $ALLPINS
	do
		echo $pin > /sys/class/gpio/export
		echo 'out' > /sys/class/gpio/gpio$pin/direction
	done
	for pin in $ALLPINS; do setpin $pin '1'; done
	sleep $SLEEPTIME
	for pin in $ALLPINS; do setpin $pin '0'; done
	sleep $SLEEPTIME
}


# ------------ MAIN -----------

startup
while `/bin/true`;
do
	read -n 1 input
	case $input in
		r )
			clear
			all_red
			;;
		g )
			clear
			all_green
			;;
		b )
			clear
			all_blue
			;;
		p )
			clear
			all_purple
			;;
		w )
			clear
			all_white
			;;
		q )
			clear
			;;
		* )
			echo "no color for that yet"
			;;
	esac
	echo '\n>'
done
