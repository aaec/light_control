#!/bin/bash

# These are functions sourced by the main script. They describe different states that
# the lights can be in. They take no params unless specified

# constants found in main file
# $ALLPINS
# $PINSFILE
# $SLEEPTIME

function clear {
	for pin in $ALLPINS
	do
		setpin $pin 0
	done
}

function all_red {
	for pin in `cat $PINSFILE | awk '$2 == "r" { print $3 }'`;
	do
		setpin $pin 1
	done
}

function all_blue {
	for pin in `cat $PINSFILE | awk '$2 == "b" { print $3 }'`;
	do
		setpin $pin 1
	done
}

function all_green {
	for pin in `cat $PINSFILE | awk '$2 == "g" { print $3 }'`;
	do
		setpin $pin 1
	done
}

function all_purple {
	for pin in `cat $PINSFILE | awk '$2 == "r" || $2 == "b" { print $3 }'`;
	do
		setpin $pin 1
	done
}

function all_white {
	for pin in `cat $PINSFILE`;
	do
		setpin $pin 1
	done
}
